require 'site_prism'
require 'capybara'

class BasePage < SitePrism::Page

  def open
    visit self.url
  end

  def refresh
    page.driver.browser.navigate.refresh
  end

  def wait_for_text(expected_text, timeout=CommonVars::DEFAULT_WAIT_TIMEOUT)
    EventuallyHelper.eventually(timeout: timeout, delay: CommonVars::DEFAULT_DELAY) do
      raise "  TEXT: '#{expected_text}' not found in #{timeout} seconds" unless self.text.include?(expected_text)
    end
  end

  def wait_for_text_absence(expected_text, timeout=CommonVars::DEFAULT_WAIT_TIMEOUT)
    EventuallyHelper.eventually(timeout: timeout, delay: CommonVars::DEFAULT_DELAY) do
      raise "  TEXT: '#{expected_text}' is present on the page, after #{timeout} seconds" if self.text.include?(expected_text)
    end
  end

  def wait_for_css(element, timeout=CommonVars::DEFAULT_WAIT_TIMEOUT)
    EventuallyHelper.eventually(timeout: timeout, delay: CommonVars::DEFAULT_DELAY) do
      raise "CSS '#{element}' not found in #{timeout} seconds" unless all(element).count > 0
    end
  end

  def wait_for_url_change(change_to, timeout=CommonVars::DEFAULT_WAIT_TIMEOUT)
    EventuallyHelper.eventually(timeout: timeout, delay: CommonVars::DEFAULT_DELAY) do
      raise "Expected '#{current_url}' url to include #{change_to}" unless current_url.include?(change_to)
    end
  end

  def should_be_loaded(timeout=CommonVars::DEFAULT_WAIT_TIMEOUT)
    EventuallyHelper.eventually(timeout: timeout, delay: CommonVars::DEFAULT_DELAY) do
      raise "Expected '#{current_url}' url to include '#{url_matcher}'" unless current_url.include?(url_matcher)
    end
  end

  def url_mather
    self.class.url_matcher || raise("Provide URL matcher for '#{self.class}'")
  end

  def take_screenshot(name)
    screen_path = "test-reports/#{name}.png"
    puts "  Screenshot: #{save_screenshot(screen_path)} saved".blue
  end

  def url_matcher
    self.class.url_matcher || raise("Provide URL matcher for '#{self.class}'")
  end
end
