require 'selenium/webdriver'
require 'capybara/cucumber'
require 'require_all'
require 'pry'

require_all 'libs'
require_all 'pages'

def web_app
  @web_app ||= WebApp.new
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.register_driver :headless_chrome do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: ['headless disable-gpu', '--window-size=1360,998'] }
  )

  Capybara::Selenium::Driver.new app,
                                 browser: :chrome,
                                 desired_capabilities: capabilities
end

Capybara.javascript_driver = :headless_chrome
Capybara.default_driver = :headless_chrome
