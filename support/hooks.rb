After do |scenario|
  if scenario.failed?
    web_app.page.take_screenshot(scenario.name.downcase.gsub(' ', '-'))
  end
end
