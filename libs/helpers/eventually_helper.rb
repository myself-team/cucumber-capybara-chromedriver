module EventuallyHelper

  # Seconds
  DEFAULT_TIMEOUT = 15
  DEFAULT_DELAY = 2

  # Waits until a block passes (stops raising an error)
  def eventually(timeout: DEFAULT_TIMEOUT, delay: DEFAULT_DELAY, &blk)
    wait_until = Time.now + timeout
    begin
      yield
    rescue Exception => e
      raise e if Time.now >= wait_until
      sleep delay
      print '.'.yellow
      retry
    end
  end

  # Waits for a block to be true
  def eventually_true(timeout: DEFAULT_TIMEOUT, delay: DEFAULT_DELAY, &blk)
    wait_until = Time.now + timeout
    while !yield
      return false if Time.now >= wait_until
      sleep delay
      print '.'.yellow
    end
    true
  end

  extend self
end
